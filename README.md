# Práctica Final: JQuery, Ajax, DOM, JSON
## (Programa de capacitación en Ingeniería de Software - DGTIC UNAM)

A continuación, se realiza una descripción de cómo se llegó a la solución del problema planteado para la actividad:
- Se eliminó la lista con el elemento "lista 1" y se dejó en su lugar únicamente el elemento *DIV*, que se utiliza para desplegar los resultados de la búsqueda.

- Al DIV anterior se le agregó el mismo valor para "class" que se usa en *index.html*, con la finalidad de que el estilo al desplegar los resultados sea el mismo.

- Al mismo DIV se le asignó "miLista" como *id*, que era el identificador original de la lista que se eliminó.

- Para agregar la funcionalidad de búsqueda se utilizó como base la función *$.ajax* del archivo *index.js*, que se coloca inmediatamente después del comentario que indica que debe comenzar a escribirse el código a partir de ese punto.

- Dentro de la función *$.ajax* utilizada como base se reemplaza la *url* con la llamada a la API necesaria para realizar la búsqueda, que queda como: 
*"http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=[numero]&query="+palabra*

- Se utiliza *append()* para agregar los elementos que coincidan con la búsqueda al *DIV* que despliega los resultados.

- Dentro de la función *crearMovieCard()* se reemplazan los textos predeterminados de calificación, descripción y género por aquellos datos que devuelve la API.

- El resto se deja tal y como aparece en la función *$.ajax* de *index.js*.