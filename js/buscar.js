$(document).ready(function(){

	var miLista = $("#miLista");
	var busqueda = $("#busqueda");

	$("#btn-buscar").on("click", function(){

		const palabra = $('#busqueda').val();
		console.log('Palabra a buscar: '+palabra);
		alert('Vamos a buscar: '+palabra);

		/*A PARTIR DE AQUÍ COMIENZA A AGREGARSE EL CÓDIGO CORRESPONDIENTE*/

	$.ajax({
		url: "http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=3356865d41894a2fa9bfa84b2b5f59bb&query="+palabra,
		success: function(respuesta) {
			console.log(respuesta);
			miLista = $('#miLista'); //DIV donde se cargará la lista de RESULTADOS

			setTimeout(function () {
				$('#loader').remove(); //Se elimina la imagen de "cargando" (los engranes)

				//Para cada elemento en la lista de resultados...
				$.each(respuesta.results, function(index, elemento) {
					//La función crearMovieCard regresa el HTML con la estructura de la pelicula
					cardHTML = crearMovieCard(elemento); 
					miLista.append(cardHTML);  // uso del método .append para agregar cada resultado obtenido con la estructura definida en cardHTML
				});
			}, 3000); //Define el tiempo que tarda en ejecutarse la función callback (3 segundos); es decir, la imagen de los engranes

		},
		error: function() {
			console.log("No se ha podido realizar la búsqueda. Por favor, intentar con otros términos de búsqueda.");
			$('#loader').remove();
			$('#miLista').html("No se ha podido realizar la búsqueda. Por favor, intentar con otros términos de búsqueda.");
		},
		beforeSend: function() { 
			//ANTES de hacer la petición se muestra la imagen de cargando.
			console.log('CARGANDO');
			$('#miLista').html('<img class="mx-auto d-block" id="loader" src="images/loading.gif" />');
		},
	});	

	});
})

function crearMovieCard(movie){
	//Llega el objeto JSON de UNA película, como la regresa la API
	console.log(movie.poster_path);
    //el atributo movie.poster_path del objeto movie, sólo contiene el nombre de la imagen (NO la ruta completa)

    //NOTAR que se accede al objeto JSON movie con la notación de punto para acceder a los atributos (movie.original_title).
	var cardHTML =
		'<!-- CARD -->'
		+'<div class="col-md-4">'
		    +'<div class="card">'
		       +'<div class="card-header">'
		          +'<img class="card-img" src="https://image.tmdb.org/t/p/w500/'+movie.poster_path+'" alt="Card image">'
		       +'</div>'
		       +'<div class="card-body">'
		          +'<h2 class="card-title">'+movie.original_title+'</h2>'
		          +'<div class="container">'
		             +'<div class="row">'
		                +'<div class="col-4 metadata">'
		                   +'<i class="fa fa-star" aria-hidden="true"></i>'
		                   +'<p>'+movie.vote_average+'</p>'
		                +'</div>'
		                +'<div class="col-8 metadata">'+movie.release_date+'</div>'
		             +'</div>'
		          +'</div>'
		          +'<p class="card-text">'+movie.overview+'</p>'
		       +'</div>'
		    +'</div>'
		+'</div>'
		+'<!-- CARD -->';

		return cardHTML;
}